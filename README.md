# MiX

Einfache WebApp die es den geneigten 2 Takt Fahrer/Benutzer möglich macht schnell seine Mischung herauszufinden.
Dazu nutze ich mal die wunderbare Eröffung von 
https://app-entwickler-verzeichnis.de/app-news/13-programmierung-a-developer-tools/582-web-app-entwicklung-mit-html5-der-grosse-praxis-guide
Da ist natürlich viel mehr drin aber als Anfang echt nicht schlecht!

Das Foto stammt von https://unsplash.com/@daniel_von_appen?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Daniel von Appen auf https://unsplash.com/collections/11423861/moped-mixes?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText Unsplash